import random
import copy
# Spaces and symbols aren't supported as input because this would drastically weaken the encryption due to their predictibility. This methods allows for 'fake' spaces in the result.
 
# The key should be a list of lists in alphabetic order, each sublist containing al chars
# pointing towards the respective position and thus character it represents.
# Example: [[n,f],[2], ...] means the first letter of the alphabet, a is represented by both n & f and b is represented by 2 only.
dkey = [['n','f'],['2'],['4'],['i','p'],['s','w'],
       ['9'],['5'],['b'],['g','a'],['d'],['x'],
       ['c','h'],['8'],['u','v'],['j'],['y'],['3'],['1','l'],
       ['o','q'],['z','m'],['e'],['k'],['7'],['0'],['6'],['r']]
       
# Example encrypted string, to use with the example key above.    
encrypteddString = "mbaqgofzsqm"

# The alphabet, required for encoding & decoding, internal order may vary.
alphabet = ['a','b','c','d','e','f','g','h','i','j','k','l','m',
            'n','o','p','q','r','s','t','u','v','w','x','y','z']    

# All of the available characters in an encrypted string
characters= ['a','b','c','d','e','f','g','h','i','j','k','l','m',
            'n','o','p','q','r','s','t','u','v','w','x','y','z',
            '0','1','2','3','4','5','6','7','8','9']    

# Decrypts the given character with the given key.
def decryptChar(char, key):
    index = 0
    for k in key:
        if char in k:
            return alphabet[index]
        else:
            index += 1
            
# Decrypts the given string with the given key.
def decrypt(string,key):
    return ''.join([decryptChar(char, key) for char in cleanString(string)])
    
# Encrypts the given character with the given key.
def encryptChar(char, key):
    # Find the index of the char we're talking about
    idx = alphabet.index(char)
    # Retrieve a list of characters it maps on to.
    chars = key[idx]
    # Pick one at random and return it.
    return chars[random.randint(0,len(chars)-1)]
    
# Encrypts the given string with the given key.
def encrypt(string, key):
    return ''.join(encryptChar(char,key) for char in cleanString(string))

# Creates a key with letters & digits. Swaps indicate how spread out the numbers will be (higher is better).
def createKey(swaps):
    # Create a list of lists (filled with the alphabet) and shuffle it
    key = [[i] for i in alphabet]
    random.shuffle(key)
    # Add numbers to the key
    for i in range(0,10):
        key[random.randint(0,25)].append(str(i))
    # Randomly swap values inside key (to spread out the numbers)
    for i in range(0,swaps):
        # Select two lists inside the key
        list1 = key[random.randint(0,25)]
        list2 = key[random.randint(0,25)]
        # Pick a value from both lists
        idx1 = random.randint(0,len(list1)-1)
        idx2 = random.randint(0,len(list2)-1)
        # Swap both values
        tmp = list1[idx1]
        list1[idx1] = list2[idx2]
        list2[idx2] = tmp
    return key

# Removes spaces and removes upper case.
def cleanString(string):
    return ''.join(c.lower() for c in string if not c.isspace())

# Creates a list of tuples (init value of 0) using the character list.
def generateCharacterTuples():
    tupleList = []
    for i in characters:
        tupleList.append((i,0))
    return tupleList

# Runs a frequency analysis:
def frequencyAnalysis(inputString):
    string = cleanString(inputString)
    freqs = generateCharacterTuples()
    for c in string:
        idx = characters.index(c)
        freqs[idx] = (c,freqs[idx][1] + 1)
    return freqs

# Nicely print a frequency analysis result.
def printFreqAnalysis(freqs):
    for i in freqs:
        print(str(i[0]) + '|' + ('X' * i[1]) + ' ' + str(i[1]))

# Nicely print a diff between two frequency analysis results.
def printFreqAnalysisDiff(f1, f2):
    totalDiff = 0
    for i in range(len(f1)):
        if (f1[i][1] > f2[i][1]):
            diff = (f1[i][1] - f2[i][1])
            rest = f2[i][1]
        else:
            diff = (f2[i][1] - f1[i][1])
            rest = f1[i][1]
        totalDiff += diff
        print(str(f1[i][0]) + '|' + ('+' * rest) + ('~' * diff) + ' ' + str(diff))
    return totalDiff

# Calculate the total diff between two frequency analysis results.
def calcFreqAnalysisDiff(f1, f2):
    totalDiff = 0
    for i in range(len(f1)):
        if (f1[i][1] > f2[i][1]):
            diff = (f1[i][1] - f2[i][1])
        else:
            diff = (f2[i][1] - f1[i][1])
        totalDiff += diff
    return totalDiff

# Use example key to decrypt an example message.
# print("Decrypt test string with test key: " + decrypt(encryptedString, dkey))

# Create a new key & encrypt a string with it;
newKey = createKey(50)
newMsg = "This is a test message"
resultE = encrypt(newMsg,newKey)
print("Encrypted string: " + resultE)
resultD = decrypt(resultE, newKey)
print("Decrypted string: " + resultD)

#print(printFreqAnalysisDiff(frequencyAnalysis(resultE), frequencyAnalysis(resultD)))




################################################################################
############################### EXPERIMENTS ####################################
################################################################################
def generateString(length):
    s = ""
    for i in range(length):
        s += alphabet[random.randint(0,25)]
    return s


def testCreateKeySwapping():
    # Doesn't compare well between runs 
    # testString = generateString(100)
    testString = "Thisisatestsdmessagetoserveasatestingmessage"
    
    # Try swapping between 0 and 'swapMax' - 1
    results = []
    swapMax = 200
    for swaps in range(swapMax):
        subResults = []
        balancingRuns = 25
        average = 0
        # Try every swapping factor 'balancingRuns' times, to average out performance
        for r in range (balancingRuns):
            key = createKey(swaps)
            encrypted = encrypt(testString,key)
            subResults.append(calcFreqAnalysisDiff(frequencyAnalysis(testString),frequencyAnalysis(encrypted)))
        # Calculate average subresult
        for i in range(len(subResults)):
             average += round(subResults[i]/balancingRuns)
             
        # Store results for this swaps run
        results.append(average)
    print (results)
testCreateKeySwapping()
